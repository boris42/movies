import React from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import styled from 'styled-components';
import { createGlobalStyle } from 'styled-components';

import MoviesComponent from './components/MoviesComponent';
import MovieDetailsComponent from './components/MovieDetailsComponent';

const GlobalStyles = createGlobalStyle`
  * {
    box-sizing: border-box;
  }
`;

const MainWrapper = styled.div`
  width: 100%;
  padding-right: 15px;
  padding-left: 15px;
  padding-top: 7.5rem;
  padding-bottom: 7.5rem;
  margin-right: auto;
  margin-left: auto;

  @media (min-width: 576px) {
    max-width: 540px;
  }

  @media (min-width: 768px) {
    max-width: 720px;
  }

  @media (min-width: 992px) {
    max-width: 960px;
  }

  @media (min-width: 1200px) {
    max-width: 1140px;
  }
`;

function App() {
  return (
    <Router>
      <GlobalStyles />
      <MainWrapper>
        <Route path="/" exact component={MoviesComponent} />
        <Route path="/movie/:id" component={MovieDetailsComponent} />
      </MainWrapper>
    </Router>
  );
}

export default App;
