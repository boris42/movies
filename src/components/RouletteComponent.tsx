import React from 'react';
import { Link } from 'react-router-dom';
import styled, { keyframes } from 'styled-components';
import { Close } from 'styled-icons/material/Close';
import { ArrowBack } from 'styled-icons/boxicons-regular/ArrowBack';

import LoadingSpinnerComponent from './LoadingSpinnerComponent';

const fadeIn = keyframes`
  from {
    opacity: 0
  }
    
  to {
    opacity: 1
  }
`;

const ModalOverlay = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  background: rgba(0, 0, 0, 0.9);
  justify-content: center;
  align-items: center;

  &.show {
    display: flex;
    animation: ${fadeIn} 0.2s ease-in-out;
  }

  &.hide {
    display: none;
  }
`;

const Modal = styled.div`
  position: fixed;
  background: white;
  width: 90%;
  box-shadow: 0 2px 6px 0 rgba(0, 0, 0, 0.2);

  @media (min-width: 576px) {
    width: 400px;
  }
`;

const ModalHeader = styled.div`
  border-bottom: 2px solid #E0E0E0;
  padding: 1rem 1.5rem;
  display: flex;
  justify-content: space-between;
  align-items: center;

  h2 {
    color: #343434;
    margin: 0;
  }
`;

const ModalBody = styled.div`
  padding: 1.5rem;
  flex-direction: column;
  justify-content: flex-start;
  align-items: flex-start;
  flex-wrap: wrap;
  height: 350px;
  overflow-y: scroll;

  @media (min-width: 768px) {
    height: 450px;
  }
`;

const ModalFooter = styled.div`
  padding: 1.5rem;
`

const ModalInput = styled.div`
  margin-bottom: 1rem;

  label {
    color: #747474;
    font-size: 18px;
  }

  input {
    margin-right: 1rem;
  }
`;

const RandomMovieWrapper = styled.div`
  padding: 1.5rem;
  width: 100%;
  height: 350px;
  overflow-y: scroll;

  img {
    display: block;
    height: 250px;
    width: 100%;
    object-fit: cover;
    object-position: 50% 0;
    margin-bottom: 1rem;
  }

  h4 {
    color: #343434;
    margin: 0 0 0.75rem 0;
  }

  p {
    color: #747474;
  }

  @media (min-width: 768px) {
    height: 450px;
  }
`;

const StyledLink = styled(Link)`
  text-decoration: none;
`;

const BackLink = styled.a`
  text-decoration: none;
  display: flex;
  align-items: center;
  color: #009a80;
  cursor: pointer;
  transition: all .2s ease-in-out;

  h2 {
    margin-left: 0.25rem;
    color: #009a80;
  }

  &:hover {
    color: 44ccaf;

    h2 {
      color: 44ccaf;
    }
  }
`;

const StyledClose = styled(Close)`
  color: #747474;
  transition: all .2s ease-in-out;

  &:hover {
    color: #343434;
  }
`

const RollButton = styled.button`
  display: block;
  background-color: #009a80;
  color: #ffffff;
  cursor: pointer;
  padding: 1rem 1.5rem;
  width: 100%;
  margin-bottom: 2rem;
  border: none;
  box-shadow: 0 2px 6px 0 rgba(0, 0, 0, 0.2);
  transition: all 0.2s ease-in-out;
  font-size: 14px;
  font-weight: 700;
  margin: 0 auto;
  outline: none;

  &:hover {
    background-color: #44ccaf;
    box-shadow: 0 2px 12px 0 rgba(0, 0, 0, 0.2);
  }
`;

const CloseButton = styled.button`
  display: block;
  outline: none;
  background-color: transparent;
  border: none;
  cursor: pointer;
`;

type Props = {
  show: boolean,
  handleClose: any
}

type RandomMovie = {
  id: number,
  poster_path: string,
  title: string,
  release_date: string,
  overview: string,
  original_language: string
}

type State = {
  genres: any[],
  selectedGenre: number;
  randomMovie: RandomMovie,
  loading: boolean
}

class RouletteComponent extends React.Component<Props, State> {
  state = {
    genres: [],
    selectedGenre: 28,
    randomMovie: {
      id: -1,
      poster_path: '',
      title: '',
      release_date: '',
      overview: '',
      original_language: ''
    },
    loading: true
  };

  componentDidMount = () => {
    document.addEventListener("keydown", this.onEscapeButton, false);

    fetch(
      'https://api.themoviedb.org/3/genre/movie/list?api_key=90decc33712cf24a95775478db99a50b&language=en-US'
    )
      .then(response => response.json())
      .then(data => {
        this.setState({ genres: data.genres, loading: false });
      });
  }

  componentWillUnmount = () => {
    document.removeEventListener("keydown", this.onEscapeButton, false);
  }

  onEscapeButton = (event: any) => {
    if(event.keyCode === 27) {
      this.props.handleClose();
    }
  }

  handleChange = (event: any) => {
    this.setState({ selectedGenre: Number(event.target.value) });
  }

  getRandomMovie = () => {
    this.setState({ loading: true });
    const randomPage = Math.floor(Math.random() * 50) + 1;

    fetch(
      `https://api.themoviedb.org/3/discover/movie?api_key=90decc33712cf24a95775478db99a50b&with_genres=${
      this.state.selectedGenre
      }&language=en-US&sort_by=popularity.desc&include_adult=false&include_video=false&page=${randomPage}`
    )
      .then(response => response.json())
      .then(data => {
        console.log(data)
        const randomMovie =
          data.results[Math.floor(Math.random() * data.results.length)];
        this.setState({ randomMovie: randomMovie, loading: false });
      });
  }

  goBack = () => {
    this.setState({
      randomMovie: {
        id: -1,
        poster_path: '',
        title: '',
        release_date: '',
        overview: '',
        original_language: ''
      }
    });
  }

  render = () => {
    const { genres, selectedGenre, randomMovie, loading } = this.state;
    const modalClass = this.props.show ? 'show' : 'hide';
    const movieLoaded = randomMovie.id > 0;

    return (
      <ModalOverlay className={modalClass}>
        {loading ? (
          <Modal>
            <LoadingSpinnerComponent />
          </Modal>
        ) : (
            <Modal>
              <ModalHeader>
                {movieLoaded ? (
                  <BackLink onClick={this.goBack}>
                    <ArrowBack size={40} />
                  </BackLink>
                ) : (
                    <h2>Movie Roulette</h2>
                  )}
                <CloseButton onClick={this.props.handleClose}>
                  <StyledClose size={40} />
                </CloseButton>
              </ModalHeader>
              <form>
                {movieLoaded ? (
                  <RandomMovieWrapper>
                    <StyledLink to={`/movie/${randomMovie.id}`}>
                      <img
                        src={`https://image.tmdb.org/t/p/w780${
                          randomMovie.poster_path
                          }`}
                        alt={randomMovie.title}
                      />
                    </StyledLink>
                    <h4>
                      {randomMovie.title} (
                    {randomMovie.release_date.split('-')[0]})
                  </h4>
                    <p>{randomMovie.overview.substring(0, 200)}...</p>
                    <p>Language: {randomMovie.original_language}</p>
                  </RandomMovieWrapper>
                ) : (
                    <ModalBody>
                      {genres.map((genre: any) => {
                        return (
                          <ModalInput key={genre.id}>
                            <label>
                              <input
                                type="radio"
                                checked={selectedGenre === genre.id}
                                value={genre.id}
                                onChange={this.handleChange}
                              />
                              {genre.name}
                            </label>
                          </ModalInput>
                        );
                      })}
                    </ModalBody>
                  )}
              </form>
              <ModalFooter>
                <RollButton onClick={this.getRandomMovie}>Roll</RollButton>
              </ModalFooter>
            </Modal>
          )}
      </ModalOverlay>
    );
  }
}

export default RouletteComponent;
