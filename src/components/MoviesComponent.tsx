import React from 'react';
import { Link } from 'react-router-dom';
import styled from 'styled-components';
import { ArrowShuffle } from 'styled-icons/typicons/ArrowShuffle';

import MovieComponent from './MovieComponent';
import RouletteComponent from './RouletteComponent';
import LoadingSpinnerComponent from './LoadingSpinnerComponent';

const MoviesWrapper = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  flex-wrap: wrap;

  @media (min-width: 1199px) {
    justify-content: space-between;
  }
`;

const LoadMoreButton = styled.button`
  display: block;
  background-color: #009a80;
  color: #ffffff;
  cursor: pointer;
  height: 75px;
  width: 75px;
  border: none;
  box-shadow: 0 2px 6px 0 rgba(0, 0, 0, 0.2);
  transition: all 0.2s ease-in-out;
  border-radius: 75px;
  font-size: 14px;
  text-transform: uppercase;
  font-weight: 700;
  margin: 0 auto;
  outline: none;

  &:hover {
    background-color: #44ccaf;
    box-shadow: 0 2px 12px 0 rgba(0, 0, 0, 0.2);
  }
`;

const MovieWrapper = styled.div`
  margin-left: 1rem;
  margin-right: 1rem;
  margin-bottom: 6rem;

  @media (min-width: 1199px) {
    margin-left: 2.5rem;
    margin-right: 2.5rem;
  }
`;

const StyledLink = styled(Link)`
  text-decoration: none;
`;

const RouletteButton = styled.button`
  position: fixed;
  bottom: 3rem;
  right: 3rem;
  display: block;
  background-color: #009a80;
  color: #ffffff;
  cursor: pointer;
  height: 75px;
  width: 75px;
  border: none;
  box-shadow: 0 2px 6px 0 rgba(0, 0, 0, 0.2);
  transition: all 0.2s ease-in-out;
  border-radius: 75px;
  font-size: 14px;
  text-transform: uppercase;
  font-weight: 700;
  margin: 0 auto;
  outline: none;

  &:hover {
    background-color: #44ccaf;
    box-shadow: 0 2px 12px 0 rgba(0, 0, 0, 0.2);
  }
`;

type State = {
  movies: any[],
  visible: number,
  nextPage: number,
  loading: boolean,
  roulette: boolean
}

class MoviesComponent extends React.Component<object, State> {
  state = {
    movies: [],
    visible: 6,
    nextPage: 3,
    loading: true,
    roulette: false
  };

  componentDidMount = () => {
    this.setState({ loading: true });
    let buffer: any[] | never[] = [];

    Promise.all([
      fetch(
        `https://api.themoviedb.org/3/discover/movie?api_key=90decc33712cf24a95775478db99a50b&language=en-US&sort_by=popularity.desc&include_adult=false&include_video=false&page=1
      `
      ),
      fetch(
        `https://api.themoviedb.org/3/discover/movie?api_key=90decc33712cf24a95775478db99a50b&language=en-US&sort_by=popularity.desc&include_adult=false&include_video=false&page=2
      `
      )
    ])
      .then(responses => {
        responses[0].json().then(data => {
          buffer = buffer.concat(data.results);
          this.setState({ movies: buffer });
        });
        responses[1].json().then(data => {
          buffer = buffer.concat(data.results);
          this.setState({ movies: buffer, loading: false });
        });
      })
      .catch(error => {
        console.log(error);
      });
  }

  loadMore = () => {
    this.setState(prevState => {
      return {
        visible: prevState.visible + 6
      };
    });

    if (this.state.visible >= 20 * (this.state.nextPage - 2)) {
      this.fetchNextPage();
    }
  }

  fetchNextPage = () => {
    fetch(
      `https://api.themoviedb.org/3/discover/movie?api_key=90decc33712cf24a95775478db99a50b&language=en-US&sort_by=popularity.desc&include_adult=false&include_video=false&page=${
      this.state.nextPage
      }`
    )
      .then(reponse => reponse.json())
      .then(data => {
        const updatedMovies = this.state.movies.concat(data.results);

        this.setState(prevState => {
          return {
            movies: updatedMovies,
            nextPage: prevState.nextPage + 1
          };
        });
      })
      .catch(error => {
        console.log(error);
      });
  }

  showRoulette = () => {
    this.setState({ roulette: true });
  }

  hideRoulette = () => {
    this.setState({ roulette: false });
  }

  render = () => {
    const { loading, movies, visible, roulette } = this.state;

    return loading ? (
      <LoadingSpinnerComponent />
    ) : (
        <div>
          <MoviesWrapper>
            {movies.slice(0, visible).map((movie: any) => {
              return (
                <MovieWrapper key={movie.id}>
                  <StyledLink to={`/movie/${movie.id}`}>
                    <MovieComponent movie={movie} />
                  </StyledLink>
                </MovieWrapper>
              );
            })}
          </MoviesWrapper>
          <LoadMoreButton onClick={this.loadMore}>Load</LoadMoreButton>
          <RouletteButton onClick={this.showRoulette}>
            <ArrowShuffle size="40" />
          </RouletteButton>
          {roulette ? (
            <RouletteComponent show={roulette} handleClose={this.hideRoulette} />
          ) : null}
        </div>
      );
  }
}

export default MoviesComponent;
