import React from 'react';
import styled from 'styled-components';
import Rater from 'react-rater';
import 'react-rater/lib/react-rater.css';

const StyledRater = styled(Rater)`
  display: flex;
  justify-content: center;
  align-items: flex-start;

  .react-rater-star {
    color: #ccccccc;
    font-size: 2rem;

    &.will-be-active {
      color: #fc6;
    }

    &.is-active {
      color: #fc6;
    }
  }
`;

type Props = {
  id: number,
  currentRating: number
}

function RatingComponent(props: Props) {
  const { currentRating } = props;

  const rate = (rating: any) => {
    const { id } = props;
    localStorage.setItem(String(id), rating);
  };
  
  return (
    <StyledRater
      total={10}
      rating={currentRating}
      onRate={rating => rate(rating.rating)}
    />
  );
}

export default RatingComponent;
