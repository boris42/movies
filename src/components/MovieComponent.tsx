import React from 'react';
import styled, { keyframes } from 'styled-components';

const fadeIn = keyframes`
  from {
    opacity: 0
  }
    
  to {
    opacity: 1
  }
`;

const Card = styled.div`
  position: relative;
  height: 425px;
  width: 275px;
  padding: 0;
  box-shadow: 0 2px 6px 0 rgba(0, 0, 0, 0.2);
  transition: all 0.2s ease-in-out;
  animation: ${fadeIn} 0.5s ease-in-out;

  &:hover {
    box-shadow: 0 2px 24px 0 rgba(0, 0, 0, 0.2);
  }
`;

const CardImage = styled.img`
  display: block;
  height: 300px;
  width: 275px;
  object-fit: none;
  object-position: 50% 0;
`;

const CardInfo = styled.div`
  overflow: hidden;
  padding: 1rem;
  height: 125px;
`;

const CardInfoTitle = styled.h4`
  color: #343434;
  margin: 0 0 0.75rem 0;
`;

const CardInfoLanguage = styled.p`
  color: #747474;
  font-size: 14px;
  margin: 0 0 1rem 0;
`;

const CardRating = styled.div`
  position: absolute;
  height: 50px;
  width: 50px;
  top: -12.5px;
  right: -10px;
  background-color: #ffffff;
  border-radius: 50px;
  border: 2px solid #44ccaf;
  box-shadow: 0 2px 6px 0 rgba(0, 0, 0, 0.2);
  display: flex;
  justify-content: center;
  align-items: center;
`;

const CardRatingText = styled.h4`
  color: #747474;
  font-size: 18px;
`;

type Props = {
  movie: any
}

function MovieComponent(props: Props) {
  const { movie } = props;
  const movieYear = movie.release_date.split('-')[0];

  return (
    <Card>
      <CardImage
        src={`https://image.tmdb.org/t/p/w300${movie.poster_path}`}
        alt={movie.original_title}
      />
      <CardInfo>
        <CardInfoTitle>
          {movie.original_title} ({movieYear})
        </CardInfoTitle>
        <CardInfoLanguage>Language: {movie.original_language}</CardInfoLanguage>
      </CardInfo>
      <CardRating>
        <CardRatingText>{movie.vote_average}</CardRatingText>
      </CardRating>
    </Card>
  );
}

export default MovieComponent;
