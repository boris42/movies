import React from 'react';
import { Link } from 'react-router-dom';
import styled from 'styled-components';
import { ArrowBack } from 'styled-icons/boxicons-regular/ArrowBack';

import RatingComponent from './RatingComponent';
import LoadingSpinnerComponent from './LoadingSpinnerComponent';

const MovieDetailsWrapper = styled.div`
`;

const MovieDetailsTitle = styled.h1`
  color: #343434;
  margin-bottom: 2rem;
`;

const MovieDetailsCoverWrapper = styled.div`
  position: relative;
  height: 525px;
  width: 100%;
`;

const MovieDetailsCoverImage = styled.img`
  display: block;
  height: 100%;
  width: 100%;
  object-fit: cover;
  box-shadow: 0 2px 6px 0 rgba(0, 0, 0, 0.2);
`;

const MovieDetailsDescription = styled.div`
  position: absolute;
  bottom: 0;
  left: 0;
  width: 80%;
  background-color: #ffffff;
  border: 2px solid #747474;
  padding: 1rem 1.5rem;
  display: flex;
  justify-content: center;
  align-items: center;

  @media (min-width: 768px) {
    width: 60%;
  }
`;

const MovieDetailsDescriptionText = styled.p`
  color: #747474;
  font-size: 16px;
  line-height: 22px;
  margin: 0;
`;

const MovieDetailsInfoWrapper = styled.div`
  display: flex;
  justify-content: space-between;
  flex-wrap: wrap;
`;

const MovieDetailsInfoGeneral = styled.div`
  width: 100%;
  padding: 1.25rem 0.25rem;

  p {
    color: #747474;
    font-size: 16px;
    line-height: 22px;

    span {
      color: #343434;
      font-weight: 700;
    }
  }

  @media (min-width: 768px) {
    width: 50%;
  }
`;
const MovieDetailsInfoRating = styled.div`
  width: 100%;
  padding: 0 0.25rem;
  display: flex;
  justify-content: flex-start;

  @media (min-width: 768px) {
    width: 50%;
    padding: 1.25rem 0.25rem;
    justify-content: flex-end;
  }
`;

const BackLink = styled(Link)`
  position: fixed;
  top: 2rem;
  left: 2rem;
  text-decoration: none;
  color: #009a80;
  font-size: 14px;
  display: flex;
  align-items: center;
  transition: all .2s ease-in-out;

  span {
    margin-left: .25rem;
  }

  &:hover {
    color: 44ccaf;
  }
`;

type Props = {
  match: any
}

type Movie = {
  id: number,
  original_title: string,
  release_date: string,
  backdrop_path: string,
  overview: string,
  vote_average: string,
  popularity: string,
  original_language: string,
  production_companies: any[]
}

type State = {
  movie: Movie,
  loading: boolean,
  rating: number
}

class MovieDetailsComponent extends React.Component<Props, State> {
  state = {
    movie: {
      id: -1,
      original_title: '',
      release_date: '',
      backdrop_path: '',
      overview: '',
      vote_average: '',
      popularity: '',
      original_language: '',
      production_companies: []
    },
    loading: true,
    rating: 0
  };

  componentDidMount = () => {
    const { id } = this.props.match.params;

    fetch(
      `https://api.themoviedb.org/3/movie/${id}?api_key=90decc33712cf24a95775478db99a50b&language=en-US`
    )
      .then(response => response.json())
      .then(data => {
        this.setState({ movie: data, loading: false });
        console.log(typeof data.id)
        if (localStorage.getItem(String(this.state.movie.id))) {
          const storageRating = Number(
            localStorage.getItem(String(this.state.movie.id))
          );
          this.setState({ rating: storageRating });
        }
      })
      .catch(error => {
        console.log(error);
      });
  }

  render = () => {
    const { movie, loading, rating } = this.state;

    return loading ? (
      <LoadingSpinnerComponent />
    ) : (
        <MovieDetailsWrapper>
          <BackLink to="/"><ArrowBack size={14} /><span>Back to Movie List</span></BackLink>
          <MovieDetailsTitle>
            {movie.original_title} ({movie.release_date.split('-')[0]})
        </MovieDetailsTitle>
          <MovieDetailsCoverWrapper>
            <MovieDetailsCoverImage
              src={`https://image.tmdb.org/t/p/w1280${movie.backdrop_path}`}
              alt={movie.original_title}
            />
            <MovieDetailsDescription>
              <MovieDetailsDescriptionText>
                {movie.overview}
              </MovieDetailsDescriptionText>
            </MovieDetailsDescription>
          </MovieDetailsCoverWrapper>
          <MovieDetailsInfoWrapper>
            <MovieDetailsInfoGeneral>
              <p>
                <span>Rating: </span>
                {movie.vote_average}
              </p>
              <p>
                <span>Popularity: </span>
                {movie.popularity}
              </p>
              <p>
                <span>Language: </span>
                {movie.original_language}
              </p>
              <p>
                <span>Production companies: </span>
                {movie.production_companies.map((company: any, index: number, arr: any[]) => {
                  if (arr.length - 1 === index) {
                    return company.name;
                  }
                  return `${company.name}, `;
                })}
              </p>
            </MovieDetailsInfoGeneral>
            <MovieDetailsInfoRating>
              <RatingComponent id={movie.id} currentRating={rating} />
            </MovieDetailsInfoRating>
          </MovieDetailsInfoWrapper>
        </MovieDetailsWrapper>
      );
  }
}

export default MovieDetailsComponent;
